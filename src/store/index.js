import { combineReducers } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import {
  createStore,
  applyMiddleware,
  getDefaultMiddleware,
} from '@reduxjs/toolkit';
// reducers
import { reducer as AppReducer } from "./app/Slices";

const rootReducer = combineReducers({
  app: AppReducer,
});

const middlewares = [...getDefaultMiddleware({
  thunk: true,
  immutableCheck: false
})];
const enhancers = [applyMiddleware(...middlewares)];

const store = createStore(rootReducer, composeWithDevTools(...enhancers));

export default store;