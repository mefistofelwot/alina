import { getItems } from "../../api/App";
import { getItemsStart, getItemsSuccess, getItemsError } from './Slices';
import Toast from 'react-native-toast-message';
import i18n from '../../localization';

export const getItemsThunk = (type, errorCalback = () => {}) => (dispatch) => {
  dispatch(getItemsStart());
  getItems(type)
    .then((res) => {
      console.log('getItems res', res)
      dispatch(getItemsSuccess(res.data.results));
    })
    .catch((err) => {
      console.log('getItems err', err)
      dispatch(getItemsError());
      errorCalback();
      Toast.show({
        type: 'error',
        position: 'bottom',
        text1: `${i18n.t('alerts.error')}...`,
        text2: `${i18n.t('alerts.error_detaile')}`,
        visibilityTime: 5000,
        autoHide: true,
      });
    });
};