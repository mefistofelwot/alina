import { createSlice } from '@reduxjs/toolkit';

export const initialState = {
	isNetwork: false,
	getItemsLoader: false,
	items: [],
};

export const appSlice = createSlice({
	name: 'app',
	initialState,
	reducers: {
		setNetworkConnect: (state, { payload }) => {
			state.isNetwork = payload;
		},
		getItemsStart: (state) => {
			state.getItemsLoader = true;
    },
		getItemsSuccess: (state, { payload }) => {
			state.getItemsLoader = false;
			state.items = payload;
    },
    getItemsError: (state) => {
			state.getItemsLoader = false;
		},
	},
});

export const {
	setNetworkConnect,
	getItemsStart, getItemsSuccess, getItemsError,
} = appSlice.actions;
export const { reducer } = appSlice;
