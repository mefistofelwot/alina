import i18n from "i18n-js";
import * as RNLocalize from "react-native-localize";
import en from "./locales/en";
import sk from "./locales/sk";

const locales = RNLocalize.getLocales();
export const country = RNLocalize.getCountry();
export const lang = locales.length > 0 ? locales[0].languageCode : 'en';

i18n.defaultLocale = "en";
i18n.fallbacks = true;
i18n.translations = {
  en,
  sk
};
i18n.locale = lang;

export default i18n;