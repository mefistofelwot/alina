export default {
  alerts: {
    no_internet_access: 'No internet access',
    error: 'Error',
    error_detaile: 'Something is goning wrong',
  },
  app: {
    categories: "Categories",
    items: 'Items',
  },
  personeDetails: {
    birth_year: "Birth Year",
  },
  planetDetails: {

  },
  starshipDetails: {

  },
};