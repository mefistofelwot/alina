import React, { memo } from "react";
import { SafeAreaView, TouchableOpacity, Image, View, Text } from "react-native";
// utils
import { ScaledSheet } from 'react-native-size-matters';
import { w, colors } from "../utils/Theme";

const Header = ({
  title = "",
  rightText = "",
  rightIcon = null,
  rightOnPress = () => null,
  leftOnPress = null,
  style = {}
}) => {
  const renderRightContent = () => {
    if (!!rightIcon) {
      return (
        <Image
          source={rightIcon}
          resizeMode='contain'
          style={styles.rightIcon}
        />
      );
    };

    return (
      <Text
        numberOfLines={1}
        style={styles.rightText}
      >
        {rightText}
      </Text>
    );
  };

  return (
    <>
    <SafeAreaView style={styles.safeArea}/>
    <View style={[styles.container, style]}>
      <TouchableOpacity
        onPress={leftOnPress}
        style={{...styles.block, alignItems: 'flex-start'}}
      >
        {
          !!leftOnPress &&
          <Image 
            resizeMode='contain'
            source={require("../assets/images/arrow-left.png")}
            style={{...styles.icon, tintColor: colors.yellow}}
          />
        }
      </TouchableOpacity>

      <View style={styles.titleContainer}>
        <Text
          numberOfLines={1}
          style={styles.titleText}
        >
          {title}
        </Text>
      </View>

      <TouchableOpacity
        onPress={rightOnPress}
        style={{...styles.block, alignItems: 'flex-end'}}
      >
        {renderRightContent()}
      </TouchableOpacity>
    </View>
    </>
  );
};

const styles = ScaledSheet.create({
  safeArea: {
    backgroundColor: colors.black
  },
  container: {
    flexDirection: "row",
    width: w(100),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  titleContainer: {
    alignItems: "center",
    justifyContent: "center",
    flex: 1
  },
  block: {
    alignItems: "center",
    justifyContent: "center",
    width: '80@ms',
    paddingHorizontal: '15@ms',
    paddingVertical: '10@ms',
  },
  icon: {
    height: '20@ms0.2',
    width: '20@ms0.2',
  },
  rightText: {
    color: colors.yellow,
    fontSize: '16@ms'
  },
  titleText: {
    color: colors.yellow,
    fontSize: '18@ms'
  },
  rightIcon: {
    width: '20@ms',
    height: '20@ms',
  }
});

export default memo(Header);