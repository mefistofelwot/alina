import React, { memo, useState } from "react";
import { Image } from "react-native";

const CustomImage = (props) => {
  const [url, setUrl] = useState(props.url);

  const onError = () => setUrl(false);

  return (
    <Image
      {...props}
      source={!!url ? { url } : require('../assets/images/crash.png')}
      onError={onError}
    />
  );
};

export default memo(CustomImage);