import React, { memo } from "react";
import { SafeAreaView, Image, Text } from "react-native";
// utils
import { ScaledSheet } from 'react-native-size-matters';
import i18n from '../../localization';

const NetInfo = ({ isNetwork = false }) => {
  if (isNetwork) return null;

  return (
    <SafeAreaView style={styles.mainContainer}>
      <Image
				source={require('../../assets/images/disconect.png')}
				resizeMode='contain'
				style={styles.icon}
			/>
      <Text style={styles.text}>
        {i18n.t('alerts.no_internet_access')}
      </Text>
    </SafeAreaView>
  );
};

const styles = ScaledSheet.create({
	mainContainer: {
    paddingHorizontal: '15@ms0.2',
    backgroundColor: '#BBBBBB',
    justifyContent: 'center',
    alignItems: "center",
    flexDirection: 'row'
	},
  text: {
    color: '#000',
    fontSize: '16@ms0.2',
    marginHorizontal: '10@ms',
    marginVertical: '10@ms',
  },
  icon: {
		width: '25@ms',
		height: '25@ms',
		tintColor: '#000'
	},
});

export default memo(NetInfo);