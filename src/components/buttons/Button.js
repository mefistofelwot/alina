import React, { memo } from "react";
import { ActivityIndicator, TouchableOpacity, View, Image, Text } from "react-native";
// utils
import { ScaledSheet } from 'react-native-size-matters';
import { colors } from '../../utils/Theme';

const Button = ({
  onPress = () => null,
  style = {},
  isLoading = false,
  title = "",
  icon = null,
  iconStyle = {},
  isSecondary = false
}) => {
  const currentColor = isSecondary ? colors.yellow : colors.black;

  const renderIcon = () => {
    if (!icon) return null;

    return (
      <Image
        source={icon}
        resizeMode='contain'
        style={iconStyle}
      />
    );
  };

  const renderContent = () => {
    if (isLoading) return <ActivityIndicator color={currentColor}/>;

    return (
      <View style={styles.textContainer}>
        {renderIcon()}
        <Text style={{...styles.text, color: currentColor}} numberOfLines={1}>
          {title}
        </Text>
      </View>
    );
  };

  return (
    <TouchableOpacity
      style={[styles.mainContainer, isSecondary ? styles.secondary : styles.primary, style]}
      onPress={onPress}
    >
      {renderContent()}
    </TouchableOpacity>
  );
};

const styles = ScaledSheet.create({
  mainContainer: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    borderRadius: '1@ms',
    width: '300@s',
    height: '45@s',
    borderRadius: '40@ms',
    borderWidth: '1@ms',
  },
  primary: {
    backgroundColor: colors.yellow,
  },
  secondary: {
    borderColor: colors.yellow,
  },
  textContainer: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "center",
    marginHorizontal: '10@ms'
  },
  text: {
    fontSize: '15@ms0.3',
    paddingVertical: '10@ms',
    fontWeight: 'bold'
  },
});

export default memo(Button);