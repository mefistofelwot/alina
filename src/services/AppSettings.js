import React, { useEffect } from "react";
// reducer
import { useDispatch } from "react-redux";
import { setNetworkConnect } from '../store/app/Slices';
// utils
import NetInfo from "@react-native-community/netinfo";

const AppSettings = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const netInfoListener = NetInfo.addEventListener(handleNetConnection);

    return () => {
      netInfoListener();
    };
  }, []);

  const handleNetConnection = (state) => dispatch(setNetworkConnect(state.isConnected));

  return null;
};

export default AppSettings;