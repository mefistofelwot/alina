export default Routes = {
  Welcome: "WELCOME",
  Categories: "CATEGORIES",
  Items: "ITEMS",
  FocusItem: "FOCUS_ITEM",
};