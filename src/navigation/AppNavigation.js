import React, { memo } from 'react';
import Welcome from '../screens/welcome/Welcome';
import Categories from '../screens/categories/Categories';
import Items from '../screens/items/Items';
import FocusItem from '../screens/focus-item/FocusItem';
// utils
import { createStackNavigator } from '@react-navigation/stack';
import Routes from './Routes';

const Stack = createStackNavigator();

const screenOptions = {
  gesturesEnabled: true,
};

const AppNavigation = () => (
  <Stack.Navigator  headerMode='none' screenOptions={screenOptions}>
    <Stack.Screen name={Routes.Welcome} component={Welcome} />
    <Stack.Screen name={Routes.Categories} component={Categories} />
    <Stack.Screen name={Routes.Items} component={Items} />
    <Stack.Screen name={Routes.FocusItem} component={FocusItem} />
  </Stack.Navigator>
);

export default memo(AppNavigation);