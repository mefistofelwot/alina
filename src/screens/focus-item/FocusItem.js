import React, { memo } from "react";
import { View, ScrollView } from "react-native";
import Header from '../../components/Header';
import PersoneDetails from './components/PersoneDetails';
import PlanetDetails from './components/PlanetDetails';
import StarshipDetails from './components/StarshipDetails';
import Image from '../../components/Image';
// utils
import { ScaledSheet } from 'react-native-size-matters';
import { getItemImage } from '../../api/App';
import { colors } from '../../utils/Theme';

const FocusItem = ({ navigation, route }) => {
  const { item, type, index } = route.params || {};

  const goBack = () => navigation.goBack();

  const renderContent = () => {
    if (type === 'people') {
      return <PersoneDetails item={item}/>
    };

    if (type === 'planets') {
      return <PlanetDetails item={item}/>
    };

    if (type === 'starships') {
      return <StarshipDetails item={item}/>
    };

    return null;
  };

  return (
    <View style={styles.mainContainer}>
      <Header
        title={item.name}
        leftOnPress={goBack}
      />
      <ScrollView
        style={styles.scrollView}
        showsVerticalScrollIndicator={false}
      >
        <Image
          uri={getItemImage(type, index + 1)}
          style={styles.itemImage}
        />
        <View style={styles.bodyContainer}>
          {renderContent()}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = ScaledSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.black,
  },
  itemImage: {
    width: "95%",
    height: "250@ms",
    marginTop: "15@ms",
    alignSelf: 'center'
  },
  bodyContainer: {
    margin: '15@ms',
    borderWidth: '1.5@ms',
    borderColor: colors.yellow,
    borderRadius: '10@ms',
    padding: '10@ms',
    width: '95%'
  },
  scrollView: {
    marginBottom: 34
  }
});

export default memo(FocusItem);