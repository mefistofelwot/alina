import React, { memo } from "react";
import { View, Text } from "react-native";
// utils
import { ScaledSheet } from 'react-native-size-matters';
import { colors } from '../../../utils/Theme';

const Row = ({ left, right, isBorder = true }) => (
  <View style={[styles.rowContainer, isBorder && styles.bottomBorder]}>
    <Text style={styles.title} numberOfLines={2}>
      {left}
    </Text>

    <Text style={styles.detail}>
      {right}
    </Text>
  </View>
);

const styles = ScaledSheet.create({
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '10@ms',
  },
  bottomBorder: {
    borderBottomColor: colors.yellow_light,
    borderBottomWidth: '1@ms'
  },
  title: {
    fontWeight: 'bold',
    color: colors.yellow,
    fontSize: '16@ms',
    flex: 1
  },
  detail: {
    color: colors.yellow,
    fontSize: '14@ms',
    flex: 1,
    textAlign: 'right'
  }
});

export default memo(Row);