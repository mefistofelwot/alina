import React, { memo } from "react";
import { View } from "react-native";
import Row from "./Row";
// utils
import i18n from '../../../localization';

const StarshipDetails = ({ item }) => (
  <View>
    <Row left={'Model'} right={item.model}/>
    <Row left={'Length'} right={item.length}/>
    <Row left={'Max atmosphering speed'} right={item.max_atmosphering_speed}/>
    <Row left={'Cost in credits'} right={item.cost_in_credits}/>
    <Row left={'Manufacturer'} right={item.manufacturer}/>
    <Row left={'Model'} right={item.model}/>
    <Row left={'Passengers'} right={item.passengers}/>
    <Row left={'Starship class'} right={item.starship_class} isBorder={false}/>
  </View>
);

export default memo(StarshipDetails);