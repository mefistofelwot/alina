import React, { memo } from "react";
import { View } from "react-native";
import Row from "./Row";
// utils
import i18n from '../../../localization';

const PlanetDetails = ({ item }) => (
  <View>
    <Row left={'Climate'} right={item.climate}/>
    <Row left={'Diameter'} right={item.diameter}/>
    <Row left={'Gravity'} right={item.gravity}/>
    <Row left={'Orbital Period'} right={item.orbital_period}/>
    <Row left={'Population'} right={item.population}/>
    <Row left={'Rotation Period'} right={item.rotation_period}/>
    <Row left={'Surface Water'} right={item.surface_water}/>
    <Row left={'Terrain'} right={item.terrain} isBorder={false}/>
  </View>
);

export default memo(PlanetDetails);