import React, { memo } from "react";
import { View } from "react-native";
import Row from "./Row";
// utils
import i18n from '../../../localization';

const PersoneDetails = ({ item }) => (
  <View>
    <Row left={i18n.t('personeDetails.birth_year')} right={item.birth_year}/>
    <Row left={'Eye Color'} right={item.eye_color}/>
    <Row left={'Gender'} right={item.gender}/>
    <Row left={'Hair Color'} right={item.hair_color}/>
    <Row left={'Height'} right={item.height}/>
    <Row left={'Mass'} right={item.mass}/>
    <Row left={'Skin Color'} right={item.skin_color} isBorder={false}/>
  </View>
);

export default memo(PersoneDetails);