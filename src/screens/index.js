import React, { memo } from "react";
import { StyleSheet, View } from "react-native";
import AppSettings from '../services/AppSettings';
import NetInfo from '../components/helpers/NetInfo';
import AppNavigation from '../navigation/AppNavigation';
import Toast from 'react-native-toast-message';
// reducer
import { useSelector, shallowEqual } from 'react-redux';

const App = () => {
  const { isNetwork } = useSelector(({ app }) => ({
    isNetwork: app.isNetwork,
  }), shallowEqual);

  return (
    <View style={styles.mainContainer}>
      <NetInfo isNetwork={isNetwork}/>
      <AppNavigation />
      <AppSettings />
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </View>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: "#fff"
  },
});

export default memo(App);