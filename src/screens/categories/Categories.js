import React, { memo } from "react";
import { View, Text, FlatList, TouchableOpacity } from "react-native";
import Header from '../../components/Header';
import Image from '../../components/Image';
// utils
import { ScaledSheet } from 'react-native-size-matters';
import Routes from '../../navigation/Routes';
import { colors } from '../../utils/Theme';
import i18n from '../../localization';

const listData = [
  {
    title: 'People',
    image: `https://starwars-visualguide.com/assets/img/characters/1.jpg`,
    type: 'people'
  },
  {
    title: 'Starships',
    image: `https://starwars-visualguide.com/assets/img/starships/5.jpg`,
    type: 'starships'
  },
  {
    title: 'Planets',
    image: `https://starwars-visualguide.com/assets/img/planets/2.jpg`,
    type: 'planets'
  },
];

const _keyExtractor = (_, idx) => String(idx);

const List = ({ navigation }) => {
  const goBack = () => navigation.goBack();
  const goToItems = (type) => navigation.navigate(Routes.Items, { type });

  const renderItem = ({ item }) => {
    const _onPress = () => goToItems(item.type);

    return (
      <TouchableOpacity
        onPress={_onPress}
        style={styles.itemContainer}
      >
        <Image
          url={item.image}
          style={styles.itemImage}
        />
        <Text style={styles.itemTitle}>
          {item.title}
        </Text>
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.mainContainer}>
      <Header
        title={i18n.t('app.categories')}
        leftOnPress={goBack}
      />
      <FlatList
        data={listData}
        keyExtractor={_keyExtractor}
        renderItem={renderItem}
        style={styles.flatList}
      />
    </View>
  );
};

const styles = ScaledSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.black,
  },
  itemContainer: {
    borderWidth: "1.5@ms",
    borderColor: colors.yellow,
    borderRadius: '10@ms',
    marginHorizontal: '15@ms',
    marginTop: '20@ms',
    padding: '10@ms',
    justifyContent: 'center',
    alignItems: "center"
  },
  itemImage: {
    width: '100%',
    height: '250@ms',
  },
  itemTitle: {
    color: colors.yellow,
    fontWeight: 'bold',
    fontSize: '18@ms',
    marginTop: '15@ms'
  },
  flatList: {
    marginBottom: 34
  }
});

export default memo(List);