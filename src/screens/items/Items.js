import React, { memo, useEffect } from "react";
import { View, Text, FlatList, TouchableOpacity, ActivityIndicator } from "react-native";
import Header from '../../components/Header';
import Image from '../../components/Image';
// reducers
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { getItemsThunk } from '../../store/app/Thunks';
import { getItemImage } from '../../api/App';
// utils
import { ScaledSheet } from 'react-native-size-matters';
import Routes from '../../navigation/Routes';
import { colors } from '../../utils/Theme';
import i18n from '../../localization';

const _keyExtractor = (_, idx) => String(idx);

const Items = ({ navigation, route }) => {
  const { type } = route.params || {};
  const dispatch = useDispatch();

  const {
    items,
    getItemsLoader
  } = useSelector(({ app }) => ({
    items: app.items,
    itgetItemsLoaderems: app.getItemsLoader,
  }), shallowEqual);

  useEffect(() => {
    dispatch(getItemsThunk(type, goBack));
  }, []);

  const goBack = () => navigation.goBack();
  const goToFocusItem = (item, index) => navigation.navigate(Routes.FocusItem, { item, type, index });

  const renderItem = ({ item, index }) => {
    const _onPress = () => goToFocusItem(item, index);

    return (
      <TouchableOpacity
        onPress={_onPress}
        style={styles.itemContainer}
      >
        <Image
          url={getItemImage(type, index + 1)}
          style={styles.itemImage}
        />
        <Text style={styles.itemName}>
          {item.name}
        </Text>
      </TouchableOpacity>
    );
  };

  const renderContent = () => {
    if (!getItemsLoader) {
      return (
        <FlatList
          data={items}
          renderItem={renderItem}
          keyExtractor={_keyExtractor}
          style={styles.flatList}
        />
      );
    } else return <ActivityIndicator size='large' color={colors.yellow} style={styles.loaderContainer}/>;
  };

  return (
    <View style={styles.mainContainer}>
      <Header
        title={i18n.t('app.items')}
        leftOnPress={goBack}
      />
      {renderContent()}
    </View>
  );
};

const styles = ScaledSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.black,
  },
  itemContainer: {
    borderWidth: "1.5@ms",
    borderColor: colors.yellow,
    borderRadius: '10@ms',
    marginHorizontal: '15@ms',
    marginTop: '20@ms',
    padding: '10@ms',
    justifyContent: 'center',
    alignItems: "center"
  },
  itemName: {
    color: colors.yellow,
    fontWeight: 'bold',
    fontSize: '18@ms',
    marginTop: '15@ms'
  },
  flatList: {
    marginBottom: 34
  },
  itemImage: {
    width: '100%',
    height: '250@ms',
  },
  loaderContainer: {
    alignSelf: "center",
    marginTop: '150@ms'
  }
});

export default memo(Items);