import React, { memo } from "react";
import { View, Image } from "react-native";
import Button from '../../components/buttons/Button';
// utils
import { ScaledSheet } from 'react-native-size-matters';
import { colors } from '../../utils/Theme';
import Routes from '../../navigation/Routes';

const Welcome = ({ navigation }) => {
  const goToCategories = () => navigation.navigate(Routes.Categories);

  return (
    <View style={styles.mainContainer}>
      <Image
        source={require('../../assets/images/logo.png')}
        resizeMode='contain'
        style={styles.logoImage}
      />
      <Button title='Get started' onPress={goToCategories} style={styles.btn}/>
    </View>
  );
};

const styles = ScaledSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: colors.black,
    justifyContent: 'center',
    alignItems: "center"
  },
  logoImage: {
    height: '150@ms',
    width: '90%'
  },
  btn: {
    marginTop: '30%'
  }
});

export default memo(Welcome);