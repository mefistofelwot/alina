import axios from "axios";

export const getItems = async (type) => {
  return axios({
    url: `https://swapi.dev/api/${type}/`,
    method: 'get',
  });
};

export const getItemImage = (type, item_id) => { // starships, planets, characters
  let _type = type;

  if (type === 'people') {
    _type = 'characters'
  };

  return `https://starwars-visualguide.com/assets/img/${_type}/${item_id}.jpg`
};