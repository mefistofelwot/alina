import { Dimensions } from "react-native";

export const { height, width } = Dimensions.get("window");
export const w = (percent) => (width * percent) / 100;
export const h = (percent) => (height * percent) / 100;

export const colors = {
  black: '#1C1E22',
  light: '#FAFAFA',
  yellow: '#FEE300',
  yellow_light: 'rgba(254, 227, 0, 0.3)',
};
