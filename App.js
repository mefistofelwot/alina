import 'react-native-gesture-handler';
import React, { memo } from 'react';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import MainApp from './src/screens';
// store
import { Provider } from 'react-redux';
import store from './src/store';
// utils
import { enableScreens } from 'react-native-screens';

enableScreens();

const App = () => (
  <Provider store={store}>
    <NavigationContainer>
      <StatusBar barStyle='light-content' backgroundColor='#1C1E22'/>
      <MainApp />
    </NavigationContainer>
  </Provider>
);

export default memo(App);